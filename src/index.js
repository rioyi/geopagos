// Dependencies
import React from 'react';
import { render } from 'react-dom';
//import { BrowserRouter as Router } from 'react-router-dom';

// Routes
// import AppRoutes from './routes';

// Assets
import './index.css';

// Data
//import db from './db.json'

// Components
import App from './App.js';

const app = document.getElementById('root');

render(
//	<Router>
//		<AppRoutes />
//	</Router>,
<App/>,
app);