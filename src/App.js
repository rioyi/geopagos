// Dependencies
import React, { Component } from 'react';
//import ReactDOM from 'react-dom';
// import PropType from 'prop-types'; 

//Components
// import Home from './components/Home/'
import ItemList from './components/ItemList/';

// Assets
import './App.css';

// App Components
class App extends Component {
  // static propTypes = {
  //   children: PropType.object.isRequired
  // };
   // <ItemList list={db} body={children} />

  render() {
//    const { children } = this.props;

    return (
      <div className="App">
        <ItemList />
      </div>
    );
  }
}

export default App;