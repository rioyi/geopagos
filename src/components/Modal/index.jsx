// Dependencies
import React, { Component } from 'react';

// Modal Component
class Modal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: '',
      cuit: '',
      number: '',
      date: '25/04/2018'
    };
  //  this.handleCancel = this.handleCancel.bind(this);
  this.handleSubmit = this.handleSubmit.bind(this);
  this.handleInput = this.handleInput.bind(this);
  }
 
handleInput(e) {
  //console.log(e.target.value, e.target.name)
  const { value, name } = e.target;
  this.setState({
    [name]: value
  })
  
};
  
  handleCancel(e) {
    e.preventDefault();
    var inputValue = e.target.value;
    var element = document.getElementById('modal');
    element.classList.add("d-none");
  }
  
  handleSubmit(e) {
    e.preventDefault();
    this.props.onAddItem(this.state);
    //console.log(this.state)
    var element = document.getElementById('modal');
    element.classList.add("d-none");
  }

  

  render() {
   // console.log(this.props.oonAddItem)
    return (
      <div className = "modal d-none" id= 'modal'>
        <div className = "modal-container">
          <div className="text text-bold modal-header">
            <div className="modal-header__content">
              Crear solicitud
            </div>
            <div className="modal-line"></div>
          </div>
          <div className="modal-content">
            <form onSubmit={this.handleSubmit}>
              <label className="modal-content__label">
                Razón Social
              </label>
              <input type="text" className="modal-content__input" onChange={this.handleInput} placeholder="ej.Nexus S.A." name="item"/>
              <div className="d-flex">  
                <div className="d-block">
                  <label className="modal-content__label modal-content__label--half">
                    Número de CUIT
                  </label>
                  <input type="text" className="modal-content__input modal-content__input--half" onChange={this.handleInput} placeholder="00-000000-0" name="cuit"/>
                </div>
                <div className="d-block">
                  <label className="modal-content__label modal-content__label--half">
                    Número de establecimiento
                  </label>
                  <input type="text" className="modal-content__input modal-content__input--half" onChange={this.handleInput} placeholder="0000000-0" name="number"/>
                </div>
              </div>
              <div className="modal-line modal-line--footer"></div>
                <div className="d-flex">
                  <button className="button-cancel" onClick={this.handleCancel}>
                    <span className="m-auto">
                      Cancelar
                    </span>
                  </button>
                  <button type="submit" className="button-accept" onSubmit={this.handleSubmit}>
                    <span className="m-auto">
                      Crear
                    </span>
                  </button>
                </div>
            </form>
            </div>
        </div>
      </div>
    );
  }
}

export default Modal;