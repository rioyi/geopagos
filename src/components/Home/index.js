// Dependencies
import React, { Component } from 'react';
import PropType from 'prop-types'; 

// Data
// import db from '../db.json';

// Assets
import visa from '../../images/visa-white.png';
import visaLogo from '../../images/visa-white-logo.png';
import arrow from '../../images/arrow.png';

// Home Components
class Home extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      // list: db.ppal_list.item_list,
      actual : {
        item: '',
        cuit: '',
        number: '',
      },
      disabled: true
    }
    this.handleButtonEnabled = this.handleButtonEnabled.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
 
 ////////////////////////////Functions///////////////////////////////

  handleButtonEnabled(e) {

    e.preventDefault();
    var inputValue = e.target.value;
    var element = document.getElementById('home-button');
    this.setState({
      name: inputValue},() => {
      if (this.state.name.length > 0) {
        element.classList.remove("disabled");
        this.setState({disabled: false});
      } else {
        element.classList.add("disabled");
        this.setState({disabled: true});
      }
    });
    
  }

  handleChange(e) {
    const target = e.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });

    console.log(this.state);
  }

  handleSubmit(e) {
    console.log('submit')
  }

  static propTypes = {
    body: PropType.object.isRequired
  };


 ////////////////////////////Render///////////////////////////////

  render() {
    const { children } = this.props;

    return (
      <div className="home__container">
        <div className="home__group home__group--1">
          <div className="home__welcome">Bienvenido.</div>
          <input type="text" placeholder=" Introduce tu nombre" onChange = {this.handleButtonEnabled} id ='username' name="username" className="home__form-input-name"/>
          <img src={arrow} className="arrow" alt="arrow"/>
          <div className="home__logo">
            <img src={visaLogo} alt="visa-logo"/>
            <div className="home__logo-text">
              Solicitudes
            </div>
          </div>
          <div className="home__form">
            <form action="/">
              <div className="home__form-group">
                <div className="home__form-title">CREA TU PRIMERA SOLICITUD</div>
                <label className="home__form-label">
                  Razón Social
                </label>
                <input type="text" className="home__form-input" placeholder="Razón social del comercio" name="item" onChange={this.handleChange}/>
                <label className="home__form-label">
                  Número de CUIT
                </label>
                <input type="number" className="home__form-input" placeholder="ej: 54654 - 54654" name="cuit" onChange={this.handleChange}/>
                <label className="home__form-label">
                  Número de establecimiento
                </label>
                <input type="number" className="home__form-input" placeholder="ej: 545566-0" name="number" onChange={this.handleChange}/>
              </div>
              <input type="button" id="home-button" className="home__form-button disabled" onClick={this.handleSubmit} disabled = {this.state.disabled} value= "Crear solicitud"/>
            </form>
          </div>
          <div className="home__credit-card">
            <div className="home__credit-card-visa">
              <img src={visa} alt="credit-card"/>
            </div>
            <div className="home__credit-card-chip">
              <div className="home__credit-card-chip--circle home__credit-card-chip--circle-1"></div>
              <div className="home__credit-card-chip--circle home__credit-card-chip--circle-2"></div>
              <div className="home__credit-card-chip--line home__credit-card-chip--line-1"></div>
              <div className="home__credit-card-chip--line home__credit-card-chip--line-2"></div>
              <div className="home__credit-card-chip--line home__credit-card-chip--line-3"></div>
              <div className="home__credit-card-chip--line home__credit-card-chip--line-4"></div>
              <div className="home__credit-card-chip--line home__credit-card-chip--line-5"></div>
              <div className="home__credit-card-chip--line home__credit-card-chip--line-6"></div>
            </div>
            <div className="home__credit-card-chip--data">
              <div className="home__credit-card-chip--data-number">
                4546 2132 5465 7852
              </div>
              <div className="home__credit-card-chip--data-name">
                NOMBRE DEL CLIENTE
              </div>
              <div className="home__credit-card-chip--data-expiration">
                03/18
              </div>
            </div>
          </div>
        </div>
        <div className="home__group home__group--2"> 
          <div className="home__title">
            Comienza a aprobar números de establecimiento
          </div>
          <div className="home__description">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus luctus quis orci eget pharetra. Pellentesque lacinia ultrices arcu, quis pulvinar eros iaculis in. Duis ut aliquam felis. 
            <br/>
            <br/>
            Donec ut tellus et leo vestibulum lobortis. Aenean dignissim varius est, nec porttitor augue aliquam vitae. Vivamus placerat nunc eu placerat maximus. Morbi tincidunt nunc eu elit porta, vel consequat leo varius. Vivamus vel ornare odio, eget feugiat purus. Aliquam erat volutpat. Vivamus a est blandit, rutrum mauris convallis, facilisis sem. Pellentesque 
            <br/>
            <br/>
            pellentesque nunc at ligula lacinia, eget porttitor mauris aliquet. Fusce non felis convallis, ultricies purus at, faucibus nisi. Sed nibh lorem, sodales ut justo sed, rutrum cursus enim. Quisque nec nibh non mi accumsan finibus. Nam iaculis eget justo pharetra finibus.
          </div>
        </div>
      </div>
    );
  }
}

export default Home;