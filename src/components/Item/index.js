// Dependencies
import React, { Component } from 'react';

// Assets
import accept_inactive from '../../images/accept_inactive.png'
import reject_inactive from '../../images/reject_inactive.png'

// Item Component
class Item extends Component {
	constructor(props) {
    super(props);
  //  this.handleCancel = this.handleCancel.bind(this);
  // this.handleSubmit = this.handleSubmit.bind(this);
	// this.handleInput = this.handleInput.bind(this);
	//this.removeItem = this.props.removeItem.bind(this);
	}
	
  render() {
		
    return (
			
      <div className="item__card" >
			<h1>{this.props.key}</h1>
				<div className="item__group">
					<div className="item__button">
	          <img src={reject_inactive} alt="logo-visa" />
					</div>
					<div className="item__group item__group--1">
						<div className="text text-bold">{this.props.item}</div>
						<div className="text text-cuil">CUIT:{this.props.cuit}</div>
					</div>
					<div className="item__group item__group--2">
						<div className="text text-number">N° de establecimiento</div>
						<div className="text text-bold">{this.props.number}</div>
						<div className="text-date__container">
							<div className="text-date">{this.props.date}</div>
						</div>
					</div>
					<div className="item__group item__group--3">
	  				<div className="text-terminal">
		  				<div className="text-center">{this.props.terminal}</div>
			  			<div className="item__line"></div>
		  			</div>
	  				<div className="item__button" onClick={this.removeItem.bind(this, key)} >
	            <img src={accept_inactive} alt="logo-visa" />
	  				</div>
					</div>
		  	</div> 
	  	</div>
    );
  }
}

export default Item;