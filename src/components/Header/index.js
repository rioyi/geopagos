// Dependencies
import React, { Component } from 'react';

// Assets
import visa from '../../images/visa.png'
import add from '../../images/add.png'

//Components
import Modal from '../Modal';

// Header Component
class Header extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    //console.log(this.props.button)
    return (
      <div className="navbar">
        <div className="navbar__logo">
          <div className="navbar__image"> 
            <img src={visa} alt="logo-visa" />
          </div>
          <div id="add" className="navbar__line"></div>
          <div className="navbar__text">{ this.props.title }</div>
        </div>    
        <a href="" className="navbar__button" id="navbar-button" onClick={this.props.onModal}>
          <img src={add} className="navbar__button--add" alt="logo" />
          <div className="navbar__button--text" >{ this.props.button }</div>
        </a>

        

      </div>
    );
  }
}

export default Header;