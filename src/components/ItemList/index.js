// Dependencies
import React, { Component } from 'react';

// Data Bases
import db from '../../db.json';

// Components
import Header from '../Header';
import Item from '../Item';
import Modal from '../Modal';

// ItemList Component
class ItemList extends Component {
	constructor() {
		super();
		this.state = {
			list: db.ppal_list.item_list,
		}
	this.handleAddItem = this.handleAddItem.bind(this);
	this.handleModal = this.handleModal.bind(this);
	this.handleRemove = this.handleRemove.bind(this);

	}

	handleModal(e) {
		//console.log('En el modal')
		e.preventDefault();
		var inputValue = e.target.value;
		var element = document.getElementById('modal');
		element.classList.remove("d-none");
	  }

	handleAddItem(item){
		//list: [...this.state.list, item]
		this.setState({
			list: [...this.state.list, item]
		})
	}

	handleRemove(index) {
		console.log(index)
	}

  render() {
	  console.log(this.state.list)
  	var list= this.state.list.map((item, i) => {
  		return (
  			<Item key= { i } item = { item.item } cuit = { item.cuit } number = { item.number } date = { item.date } terminal = { item.terminal } 
				removeItem = {this.handleRemove} />
  			)
	  	}
	  )
	  console.log(list.reverse())
	  return (
		
	  	<div className="item-content">
	  		<Header name="xxxxx" title={db.ppal_list.header.title} button={db.ppal_list.header.button} onModal={this.handleModal}/>
		  	<div className="item-list">
				  {list}
				  
		  	</div>

			<Modal onAddItem={this.handleAddItem} />
			  
	  	</div>
	  );
  }
}

export default ItemList;