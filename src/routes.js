// Dependencies
import React from 'react';
import { Route, Switch } from 'react-router-dom';

// Components
import App from './App.js';
import Home from './components/Home/';
import ItemList from './components/ItemList/';

const AppRoutes = () => 

	<App>
		<Switch>
			<Route path="/" component={App} />
			<Route path="/solicitudes" component={ItemList} />
		</Switch>
	</App>;

export default AppRoutes;